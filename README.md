# Revenue Quebec Assign Agent

This project is an example of assigning a Review Agent to a task based on them working on that customer previously.

See the folder rq-reviewer for a BPM Studio Application.

# UDAs

ClientId: This is a unique ID for each customer.
Notice it is a worklist UDA because we want to be able to search based on this value.

AssignedAgent: This is the name of the Review Agent that handled the Review Activity.
By default this is set to a string NULL so we know it hasn't been set.
We can't use blank string.
Notice it is a worklist UDA because we want to search for process instances that have this set.

ProcessIdentifier: This is a unique name for this specific Process.
We could also use the Process name but we assume that may change.
Notice it is a worklist UDA because we want to search for these types of process instances.

InfoJson: This is the raw JSON result of our REST call to get the list of previous process instances for this customer.

piList: This is array of process instances inside InfoJson.

# BPM Actions

The Review Activity uses two Role Actions and an Epilogue Action.

## Get PIs with Filter (Role Action)
The first action fetches the previous process instances.
It gets a list of Process Instances with the correct ProcessIdentifier and ClientId.
It also only gets Process Instances where AssignedAgent is not NULL.
Finally, it sorts the list so that the most recent Process Instance is first in the list.

## Assign Agent (Role Action)
The Assign Agent Action assigns an Agent only when the following conditions are met.
Otherwise it does nothing and the task will get assigned to the role by default.
1) There must be at least one previous Process Instance.
2) Check the AssignedAgent a verify that it is in the list of current assignees.

## Get Agent (Epilogue Action)
This action sets AssignedAgent based on the agent that completed the Review Activity.
